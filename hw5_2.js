
`use strict`

       let newUser = {

            createNewUser(){

                this._firstName = prompt(`What is your first name?`);
                this._lastName = prompt(`What is your last name?`);
                return newUser;
            },

            getLogin(){

                login = this._firstName[0]+this._lastName;
                this.login = login.toLowerCase();
                return login;
            },

            set(value){
                this._firstName = value;
            },

            set(value){
                this._lastName = value;
            },

        };

        newUser.createNewUser();
        newUser.getLogin();

        Object.defineProperty(newUser, `_firstName`, {
            writable: false,
            configurable: false,
        });

        Object.defineProperty(newUser, `_lastName`, {
            writable: false,
            configurable: false,
        });

        console.log(newUser);
        console.log(newUser.login);

        // **********варіант 2 **********

    function createNewUser(){

        let newUser = {

            _firstName: prompt(`What is your first name?`),
            _lastName: prompt(`What is your last name?`),
            login:"",

            getLogin(){

                login = this._firstName[0]+this._lastName;
                    this.login = login.toLowerCase();
                    return login;
                },

                set(value){
                    this._firstName = value;
                },

                set(value){
                    this._lastName = value;
                },
        };

        newUser.getLogin();

        Object.defineProperty(newUser, `_firstName`, {
            writable: false,
            configurable: false,
        });

        Object.defineProperty(newUser, `_lastName`, {
            writable: false,
            configurable: false,
        });

        console.log(newUser);
        console.log(newUser.login);

        return newUser;
    };

    createNewUser();
